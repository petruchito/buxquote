var path_hash_sbrf={};
var path_hash_moex={};
var finished_flag = 0;

path_hash_moex['usdrub_tom'] = 'tr[data-title="USDRUB_TOM"] > td[data-name="LAST"] > span';
path_hash_moex['usdrub_tom_date'] = 'tr[data-title="USDRUB_TOM"] > td[data-name="SETTLEDATE"] > span';
path_hash_moex['usdrub_tom_change'] = 'tr[data-title="USDRUB_TOM"] > td[data-name="CHANGE"] > span';
path_hash_moex['usdrub_tom_time'] = 'tr[data-title="USDRUB_TOM"] > td[data-name="TIME"] > span';

path_hash_moex['usdrub_tod_date'] = 'tr[data-title="USDRUB_TOD"] > td[data-name="SETTLEDATE"] > span';
path_hash_moex['usdrub_tod_change'] = 'tr[data-title="USDRUB_TOD"] > td[data-name="CHANGE"] > span';
path_hash_moex['usdrub_tod_time'] = 'tr[data-title="USDRUB_TOD"] > td[data-name="TIME"] > span';
path_hash_moex['usdrub_tod'] = 'tr[data-title="USDRUB_TOD"] > td[data-name="LAST"] > span';

path_hash_sbrf['usd_sbrf_from_datetime'] = '.r_g_col5 > h3';
path_hash_sbrf['usd_sbrf_buy'] = '.r_g_col5 > table > tbody > tr:nth-child(2) > td:nth-child(3)';
path_hash_sbrf['usd_sbrf_sell'] = '.r_g_col5 > table > tbody > tr:nth-child(2) > td:nth-child(5)';

function getvalue(path,page) {
	return page.evaluate(function(path) {
		return document.querySelector(path).innerText;
	},path);
}




function waitFor(testFx, onReady, timeOutMillis) {
	var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
	    start = new Date().getTime(),
	    condition = false,
	    interval = setInterval(function() {
		    if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
			    // If not time-out yet and condition not yet fulfilled
			    condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
		    } else {
			    if(!condition) {
				    // If condition still not fulfilled (timeout but condition is 'false')
				    console.log("'waitFor()' timeout");
				    phantom.exit(1);
			    } else {
				    // Condition fulfilled (timeout and/or condition is 'true')
//				    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
				    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
				    clearInterval(interval); //< Stop this interval
			    }
		    }
	    }, 250); //< repeat check every 250ms
};





//============MOEX==============//
var page_moex = require('webpage').create();


phantom.addCookie({
	'name'     : 'disclaimerAgreementCookie',   
	'value'    : '1',  
	'domain'   : 'moex.ru',
	'path'     : '/',                
	'httponly' : true,
	'secure'   : false,
	'expires'  : (new Date()).getTime() + (1000 * 60 * 60 * 24 * 365)   
}); 


finished_flag++;
page_moex.open('http://moex.com/ru/markets/currency/', function(status) {
		console.log("Status_moex: " + status);


		if (status == "success") {


			page_moex.evaluate(function() {
				$("button").click();
			});

			waitFor( function () {
				return page_moex.evaluate( function () {
					return $('td[data-name="LAST"]').is(':visible');
				});

			}, function () { 

				var quotes={};
				for (var k in path_hash_moex) {
					if (path_hash_moex.hasOwnProperty(k)) {
						quotes[k] = getvalue(path_hash_moex[k],page_moex);
					};
				};

				for (var k in path_hash_moex) {
					if (path_hash_moex.hasOwnProperty(k)) {
						console.log(k+": "+quotes[k]);
					};
				};


				//page_moex.render('moex.png');

				finished_flag--;


			});
		};

});


//============SBRF==============//

var page_sbrf = require('webpage').create();

finished_flag++;
page_sbrf.open('http://data.sberbank.ru/moscow/ru/quotes/currencies/?base=beta', function(status) {
		console.log("Status_sbrf: " + status);

		if (status == "success") {


			var quotes={};
			for (var k in path_hash_sbrf) {
				if (path_hash_sbrf.hasOwnProperty(k)) {
					quotes[k] = getvalue(path_hash_sbrf[k],page_sbrf);
				};
			};
			var extract_date = /(\d{2}\.\d{2}\.\d{4})\s+(\d{2}\:\d{2})/; //����� ����������� ����� � 04.04.2015 00:00 �� �������� �������
			
			var matches = extract_date.exec(quotes['usd_sbrf_from_datetime'].toString());
			quotes['usd_sbrf_from_date'] = matches[1];
			quotes['usd_sbrf_from_time'] = matches[2];


			for (var k in quotes) {
				if (quotes.hasOwnProperty(k)) {
					console.log(k+": "+quotes[k]);
				};
			};


//			page_sbrf.render('sbrf.png');

			finished_flag--;


		};

});

//============CBR==============//
finished_flag++;
var today = new Date();
var today_date = ('0' + today.getDate()).slice(-2) + '.'
+ ('0' + (today.getMonth()+1)).slice(-2) + '.'
+ today.getFullYear();

var page_cbr = require('webpage').create();
page_cbr.open('http://www.cbr.ru/currency_base/daily.aspx?date_req='+today_date, function(status) {

		console.log("Status_cbr: " + status);


		if (status == "success") {

			var usd_cbr = page_cbr.evaluate(function() {
				return $('.data > tbody > tr > td:contains("USD") + td+td+td').text(); 
			});

			console.log('usd_cbr: '+usd_cbr);
			console.log('usd_cbr_date: '+today_date);



//			page_cbr.render('cbr.png');
			finished_flag--;
		};

});

waitFor ( function () {
	//console.log('F: '+finished_flag); 
	if (finished_flag == 0) { 
		return true 
	} 
}, function () { 
	//console.log('EXIT: '+finished_flag); 
	phantom.exit();
		}, 10000);
