﻿namespace BuxQuote
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnRefresh = new System.Windows.Forms.Button();
            this.usd_cbr = new System.Windows.Forms.TextBox();
            this.usd_cbr_date = new System.Windows.Forms.TextBox();
            this.usd_sbrf_from_time = new System.Windows.Forms.TextBox();
            this.usdrub_tom = new System.Windows.Forms.TextBox();
            this.usd_sbrf_from_date = new System.Windows.Forms.TextBox();
            this.usdrub_tom_date = new System.Windows.Forms.TextBox();
            this.usd_sbrf_buy = new System.Windows.Forms.TextBox();
            this.usdrub_tom_change = new System.Windows.Forms.TextBox();
            this.usdrub_tom_time = new System.Windows.Forms.TextBox();
            this.usdrub_tod = new System.Windows.Forms.TextBox();
            this.usdrub_tod_date = new System.Windows.Forms.TextBox();
            this.usdrub_tod_change = new System.Windows.Forms.TextBox();
            this.usdrub_tod_time = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.linkSbrf = new System.Windows.Forms.LinkLabel();
            this.linkCBR = new System.Windows.Forms.LinkLabel();
            this.linkTOD = new System.Windows.Forms.LinkLabel();
            this.linkTOM = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(457, 244);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(100, 23);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.button1_Click);
            // 
            // usd_cbr
            // 
            this.usd_cbr.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usd_cbr.Cursor = System.Windows.Forms.Cursors.Default;
            this.usd_cbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usd_cbr.Location = new System.Drawing.Point(245, 201);
            this.usd_cbr.Name = "usd_cbr";
            this.usd_cbr.ReadOnly = true;
            this.usd_cbr.ShortcutsEnabled = false;
            this.usd_cbr.Size = new System.Drawing.Size(100, 22);
            this.usd_cbr.TabIndex = 3;
            this.usd_cbr.TabStop = false;
            this.usd_cbr.Text = "usd_cbr";
            this.usd_cbr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usd_cbr_date
            // 
            this.usd_cbr_date.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usd_cbr_date.Cursor = System.Windows.Forms.Cursors.Default;
            this.usd_cbr_date.Location = new System.Drawing.Point(351, 201);
            this.usd_cbr_date.Name = "usd_cbr_date";
            this.usd_cbr_date.ReadOnly = true;
            this.usd_cbr_date.ShortcutsEnabled = false;
            this.usd_cbr_date.Size = new System.Drawing.Size(100, 20);
            this.usd_cbr_date.TabIndex = 3;
            this.usd_cbr_date.TabStop = false;
            this.usd_cbr_date.Text = "usd_cbr_date";
            this.usd_cbr_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usd_sbrf_from_time
            // 
            this.usd_sbrf_from_time.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usd_sbrf_from_time.Cursor = System.Windows.Forms.Cursors.Default;
            this.usd_sbrf_from_time.Location = new System.Drawing.Point(457, 161);
            this.usd_sbrf_from_time.Name = "usd_sbrf_from_time";
            this.usd_sbrf_from_time.ReadOnly = true;
            this.usd_sbrf_from_time.ShortcutsEnabled = false;
            this.usd_sbrf_from_time.Size = new System.Drawing.Size(100, 20);
            this.usd_sbrf_from_time.TabIndex = 3;
            this.usd_sbrf_from_time.TabStop = false;
            this.usd_sbrf_from_time.Text = "usd_sbrf_from_time";
            this.usd_sbrf_from_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tom
            // 
            this.usdrub_tom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tom.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usdrub_tom.Location = new System.Drawing.Point(245, 89);
            this.usdrub_tom.Name = "usdrub_tom";
            this.usdrub_tom.ReadOnly = true;
            this.usdrub_tom.ShortcutsEnabled = false;
            this.usdrub_tom.Size = new System.Drawing.Size(100, 22);
            this.usdrub_tom.TabIndex = 3;
            this.usdrub_tom.TabStop = false;
            this.usdrub_tom.Text = "usdrub_tom";
            this.usdrub_tom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usd_sbrf_from_date
            // 
            this.usd_sbrf_from_date.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usd_sbrf_from_date.Cursor = System.Windows.Forms.Cursors.Default;
            this.usd_sbrf_from_date.Location = new System.Drawing.Point(351, 161);
            this.usd_sbrf_from_date.Name = "usd_sbrf_from_date";
            this.usd_sbrf_from_date.ReadOnly = true;
            this.usd_sbrf_from_date.ShortcutsEnabled = false;
            this.usd_sbrf_from_date.Size = new System.Drawing.Size(100, 20);
            this.usd_sbrf_from_date.TabIndex = 3;
            this.usd_sbrf_from_date.TabStop = false;
            this.usd_sbrf_from_date.Text = "usd_sbrf_from_date";
            this.usd_sbrf_from_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tom_date
            // 
            this.usdrub_tom_date.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tom_date.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tom_date.Location = new System.Drawing.Point(351, 89);
            this.usdrub_tom_date.Name = "usdrub_tom_date";
            this.usdrub_tom_date.ReadOnly = true;
            this.usdrub_tom_date.ShortcutsEnabled = false;
            this.usdrub_tom_date.Size = new System.Drawing.Size(100, 20);
            this.usdrub_tom_date.TabIndex = 3;
            this.usdrub_tom_date.TabStop = false;
            this.usdrub_tom_date.Text = "usdrub_tom_date";
            this.usdrub_tom_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usd_sbrf_buy
            // 
            this.usd_sbrf_buy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usd_sbrf_buy.Cursor = System.Windows.Forms.Cursors.Default;
            this.usd_sbrf_buy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usd_sbrf_buy.Location = new System.Drawing.Point(245, 161);
            this.usd_sbrf_buy.Name = "usd_sbrf_buy";
            this.usd_sbrf_buy.ReadOnly = true;
            this.usd_sbrf_buy.ShortcutsEnabled = false;
            this.usd_sbrf_buy.Size = new System.Drawing.Size(100, 22);
            this.usd_sbrf_buy.TabIndex = 3;
            this.usd_sbrf_buy.TabStop = false;
            this.usd_sbrf_buy.Text = "usd_sbrf_buy";
            this.usd_sbrf_buy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tom_change
            // 
            this.usdrub_tom_change.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tom_change.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tom_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usdrub_tom_change.Location = new System.Drawing.Point(351, 115);
            this.usdrub_tom_change.Name = "usdrub_tom_change";
            this.usdrub_tom_change.ReadOnly = true;
            this.usdrub_tom_change.ShortcutsEnabled = false;
            this.usdrub_tom_change.Size = new System.Drawing.Size(56, 20);
            this.usdrub_tom_change.TabIndex = 3;
            this.usdrub_tom_change.TabStop = false;
            this.usdrub_tom_change.Text = "usdrub_tom_change";
            this.usdrub_tom_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // usdrub_tom_time
            // 
            this.usdrub_tom_time.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tom_time.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tom_time.Location = new System.Drawing.Point(457, 89);
            this.usdrub_tom_time.Name = "usdrub_tom_time";
            this.usdrub_tom_time.ReadOnly = true;
            this.usdrub_tom_time.ShortcutsEnabled = false;
            this.usdrub_tom_time.Size = new System.Drawing.Size(100, 20);
            this.usdrub_tom_time.TabIndex = 3;
            this.usdrub_tom_time.TabStop = false;
            this.usdrub_tom_time.Text = "usdrub_tom_time";
            this.usdrub_tom_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tod
            // 
            this.usdrub_tod.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tod.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usdrub_tod.Location = new System.Drawing.Point(245, 25);
            this.usdrub_tod.Name = "usdrub_tod";
            this.usdrub_tod.ReadOnly = true;
            this.usdrub_tod.ShortcutsEnabled = false;
            this.usdrub_tod.Size = new System.Drawing.Size(100, 22);
            this.usdrub_tod.TabIndex = 3;
            this.usdrub_tod.TabStop = false;
            this.usdrub_tod.Text = "usdrub_tod";
            this.usdrub_tod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tod_date
            // 
            this.usdrub_tod_date.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tod_date.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tod_date.Location = new System.Drawing.Point(351, 25);
            this.usdrub_tod_date.Name = "usdrub_tod_date";
            this.usdrub_tod_date.ReadOnly = true;
            this.usdrub_tod_date.ShortcutsEnabled = false;
            this.usdrub_tod_date.Size = new System.Drawing.Size(100, 20);
            this.usdrub_tod_date.TabIndex = 3;
            this.usdrub_tod_date.TabStop = false;
            this.usdrub_tod_date.Text = "usdrub_tod_date";
            this.usdrub_tod_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // usdrub_tod_change
            // 
            this.usdrub_tod_change.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tod_change.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tod_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usdrub_tod_change.Location = new System.Drawing.Point(351, 51);
            this.usdrub_tod_change.Name = "usdrub_tod_change";
            this.usdrub_tod_change.ReadOnly = true;
            this.usdrub_tod_change.ShortcutsEnabled = false;
            this.usdrub_tod_change.Size = new System.Drawing.Size(56, 20);
            this.usdrub_tod_change.TabIndex = 3;
            this.usdrub_tod_change.TabStop = false;
            this.usdrub_tod_change.Tag = "";
            this.usdrub_tod_change.Text = "usdrub_tod_change";
            this.usdrub_tod_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // usdrub_tod_time
            // 
            this.usdrub_tod_time.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.usdrub_tod_time.Cursor = System.Windows.Forms.Cursors.Default;
            this.usdrub_tod_time.Location = new System.Drawing.Point(457, 25);
            this.usdrub_tod_time.Name = "usdrub_tod_time";
            this.usdrub_tod_time.ReadOnly = true;
            this.usdrub_tod_time.ShortcutsEnabled = false;
            this.usdrub_tod_time.Size = new System.Drawing.Size(100, 20);
            this.usdrub_tod_time.TabIndex = 3;
            this.usdrub_tod_time.TabStop = false;
            this.usdrub_tod_time.Text = "usdrub_tod_time";
            this.usdrub_tod_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Биржевой курс на торговой сессии:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Курс Сбербанка России";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Курс Центрального банка России:";
            // 
            // linkSbrf
            // 
            this.linkSbrf.AutoSize = true;
            this.linkSbrf.Location = new System.Drawing.Point(194, 164);
            this.linkSbrf.Name = "linkSbrf";
            this.linkSbrf.Size = new System.Drawing.Size(46, 13);
            this.linkSbrf.TabIndex = 9;
            this.linkSbrf.TabStop = true;
            this.linkSbrf.Text = "Ссылка";
            this.linkSbrf.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSbrf_LinkClicked);
            // 
            // linkCBR
            // 
            this.linkCBR.AutoSize = true;
            this.linkCBR.Location = new System.Drawing.Point(194, 204);
            this.linkCBR.Name = "linkCBR";
            this.linkCBR.Size = new System.Drawing.Size(46, 13);
            this.linkCBR.TabIndex = 9;
            this.linkCBR.TabStop = true;
            this.linkCBR.Text = "Ссылка";
            this.linkCBR.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCBR_LinkClicked);
            // 
            // linkTOD
            // 
            this.linkTOD.AutoSize = true;
            this.linkTOD.Location = new System.Drawing.Point(209, 28);
            this.linkTOD.Name = "linkTOD";
            this.linkTOD.Size = new System.Drawing.Size(30, 13);
            this.linkTOD.TabIndex = 10;
            this.linkTOD.TabStop = true;
            this.linkTOD.Text = "TOD";
            this.linkTOD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkTOD_LinkClicked);
            // 
            // linkTOM
            // 
            this.linkTOM.AutoSize = true;
            this.linkTOM.Location = new System.Drawing.Point(210, 94);
            this.linkTOM.Name = "linkTOM";
            this.linkTOM.Size = new System.Drawing.Size(31, 13);
            this.linkTOM.TabIndex = 10;
            this.linkTOM.TabStop = true;
            this.linkTOM.Text = "TOM";
            this.linkTOM.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkTOM_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(410, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "%";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(410, 118);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "%";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 279);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkTOM);
            this.Controls.Add(this.linkTOD);
            this.Controls.Add(this.linkCBR);
            this.Controls.Add(this.linkSbrf);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.usd_cbr_date);
            this.Controls.Add(this.usdrub_tod);
            this.Controls.Add(this.usdrub_tod_time);
            this.Controls.Add(this.usdrub_tom_time);
            this.Controls.Add(this.usdrub_tod_change);
            this.Controls.Add(this.usdrub_tom_change);
            this.Controls.Add(this.usdrub_tod_date);
            this.Controls.Add(this.usdrub_tom_date);
            this.Controls.Add(this.usd_sbrf_buy);
            this.Controls.Add(this.usd_sbrf_from_date);
            this.Controls.Add(this.usdrub_tom);
            this.Controls.Add(this.usd_sbrf_from_time);
            this.Controls.Add(this.usd_cbr);
            this.Controls.Add(this.btnRefresh);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "BuxQuote";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TextBox usd_cbr;
        private System.Windows.Forms.TextBox usd_cbr_date;
        private System.Windows.Forms.TextBox usd_sbrf_from_time;
        private System.Windows.Forms.TextBox usdrub_tom;
        private System.Windows.Forms.TextBox usd_sbrf_from_date;
        private System.Windows.Forms.TextBox usdrub_tom_date;
        private System.Windows.Forms.TextBox usd_sbrf_buy;
        private System.Windows.Forms.TextBox usdrub_tom_change;
        private System.Windows.Forms.TextBox usdrub_tom_time;
        private System.Windows.Forms.TextBox usdrub_tod;
        private System.Windows.Forms.TextBox usdrub_tod_date;
        private System.Windows.Forms.TextBox usdrub_tod_change;
        private System.Windows.Forms.TextBox usdrub_tod_time;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkSbrf;
        private System.Windows.Forms.LinkLabel linkCBR;
        private System.Windows.Forms.LinkLabel linkTOD;
        private System.Windows.Forms.LinkLabel linkTOM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

