﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;

namespace BuxQuote
{
    public partial class MainForm : Form
    {
        delegate void setTextCallback(string[] text);
        delegate void exitedHandlerCallback();

        private bool Active = false;
        private bool wasCancelled = false;

        public MainForm()
        {
            InitializeComponent();



        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (this.Active)
            {
                if (this.exeProcess != null || !(this.exeProcess.HasExited))
                {
                    exeProcess.Kill();

                    this.Active = false; btnRefresh.Text = "Обновить"; this.wasCancelled = true;
                }
            }
            else
            {
                this.Active = true; btnRefresh.Text = "Отмена";
                //    btnRefresh.Enabled = false;
                foreach (TextBox tb in this.Controls.OfType<TextBox>())
                {

                    //tb.ReadOnly = true;
                    tb.ForeColor = SystemColors.ControlLight;
                    tb.BackColor = SystemColors.ControlDark;
                    this.Refresh();

                }
                LaunchPhantom();

            }


        }


        private void MainForm_Shown(object sender, EventArgs e)
        {
            btnRefresh.PerformClick();

            //LaunchPhantom();
        }

        private void exited_handler(object sendingProcess, System.EventArgs e)
        {
            this.Active = false;
            
            enableBtnRefresh();
        }

        private void enableBtnRefresh()
        {
            if (this.usd_cbr.InvokeRequired)
            {
                exitedHandlerCallback d = new exitedHandlerCallback(enableBtnRefresh);
                this.Invoke(d);
            }
            else
            {
                btnRefresh.Text = "Обновить";
                if (!wasCancelled)
                {
                    try
                    {

                        SortedDictionary<float, string> quotes = new SortedDictionary<float, string>();
                        quotes.Add(Single.Parse(usdrub_tod.Text), "usdrub_tod");
                        quotes.Add(Single.Parse(usdrub_tom.Text), "usdrub_tom");
                        quotes.Add(Single.Parse(usd_sbrf_buy.Text), "usd_sbrf_buy");
                        quotes.Add(Single.Parse(usd_cbr.Text), "usd_cbr");

                        this.Controls[quotes.Last().Value].BackColor = Color.LightGoldenrodYellow;
                    } catch { }

                }
                this.wasCancelled = false;
            }
        }

        private void parse_output_handler(object sendingProcess, DataReceivedEventArgs output)
        {
            if (!String.IsNullOrEmpty(output.Data))
            {

                string[] values = output.Data.Split(new Char[] { ':' }, 2);
                this.setText(values);

            }
        }



        private void setText(string[] values)
        {
            var lbl = this.Controls[values[0].Trim()];
            if (this.usd_cbr.InvokeRequired)
            {
                setTextCallback d = new setTextCallback(setText);

                this.Invoke(d, new object[] { values });
            }
            else
            {
                if (lbl != null)
                {
                    lbl.BackColor = SystemColors.ControlLightLight;
                    lbl.ForeColor = SystemColors.WindowText;

                    lbl.Text = values[1].Trim();
                    lbl.Refresh();
                }
            }

        }
        private Process exeProcess;
        private void LaunchPhantom()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "phantomjs.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "--load-images=false --cookies-file=cookies.txt quotes.js";
            startInfo.RedirectStandardOutput = true;


            this.exeProcess = Process.Start(startInfo);

            exeProcess.EnableRaisingEvents = true;
            exeProcess.OutputDataReceived += new DataReceivedEventHandler(parse_output_handler);
            exeProcess.Exited += new EventHandler(exited_handler);
            exeProcess.BeginOutputReadLine();

        }


        private void linkSbrf_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://data.sberbank.ru/moscow/ru/quotes/currencies/?base=beta");
        }

        private void linkCBR_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.cbr.ru/currency_base/daily.aspx?date_req=" + DateTime.Now.ToString("dd.MM.yyyy"));

        }

        private void linkTOD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://moex.com/ru/issue/USD000000TOD/CETS");
        }

        private void linkTOM_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://moex.com/ru/issue/USD000UTSTOM/CETS");
        }




    }
}
